#include <PA9.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "music1.h"
#include "moin.h"
#include "pluss.h"
#include "perduu.h"
#include "gagnee.h"

#define temps 60

#include "gfx/all_gfx.h"

int main(int argc, char ** argv)
{
	int continuer = 1, nb = 0, tmp = 0, tmpP = 0, ch = 0, etat = 0, cb = 0;
	char total[4] = {""};
	sprintf(total, "");
	
	PA_Init();   
	AS_Init(AS_MODE_MP3 | AS_MODE_SURROUND | AS_MODE_16CH);
	AS_SetDefaultSettings(AS_PCM_8BIT, 11025, AS_SURROUND);
	PA_InitVBL();
	PA_InitText(1, 1);
	PA_InitText(0, 1);
	PA_SetTextTileCol(0, TEXT_YELLOW);
	PA_SetTextTileCol(1, TEXT_YELLOW);
	PA_InitRand ();
	PA_VBLCounterStart(0);

	PA_LoadBackground(1, 0, &intro);
	PA_LoadBackground(0, 0, &intro);
	PA_LoadSpritePal(0, 0, (void*)_0_Pal);
	PA_LoadSpritePal(0, 1, (void*)_1_Pal);
	PA_LoadSpritePal(0, 2, (void*)_2_Pal);
	PA_LoadSpritePal(0, 3, (void*)_3_Pal);
	PA_LoadSpritePal(0, 4, (void*)_4_Pal);
	PA_LoadSpritePal(0, 5, (void*)_5_Pal);
	PA_LoadSpritePal(0, 6, (void*)_6_Pal);
	PA_LoadSpritePal(0, 7, (void*)_7_Pal);
	PA_LoadSpritePal(0, 8, (void*)_8_Pal);
	PA_LoadSpritePal(0, 9, (void*)_9_Pal);
	PA_LoadSpritePal(0, 10, (void*)oui_Pal);
	PA_LoadSpritePal(0, 11, (void*)non_Pal);
	AS_MP3DirectPlay((u8*)music1, (u32)music1_size);
	
	while (continuer)
	{
		tmp = PA_VBLCounter[0]/60;
		if (tmp - tmpP >= 6 && etat == 0)
		{
			PA_DeleteBg(1, 0);
			PA_DeleteBg(0, 0);
			PA_LoadBackground(1, 0, &expliq);
			tmpP = tmp;
			etat = 1;
			PA_CreateSprite(0, 10, (void*)oui_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 64);
			PA_CreateSprite(0, 11, (void*)non_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 128);
		}
		
		if (PA_SpriteTouched(10) && etat == 1 || PA_SpriteTouched(10) && etat == 4)
		{
			PA_ClearTextBg(0);
			PA_SetTextTileCol(1, TEXT_YELLOW);
			AS_MP3Stop();
			PA_DeleteBg(1, 0);
			PA_VBLCounter[1] = 0;
			PA_VBLCounterStart(1);
			nb = PA_RandMinMax(10000, 50000);
			PA_OutputText(1, 7, 10, "C'est parti!!!");
			etat = 2;
			cb = 0;
			ch = 0;
			sprintf(total, "");
		}
		
		if (etat == 2)
		{		
			PA_DeleteBg(1, 2);
			PA_DeleteBg(0, 0);
			PA_DeleteSprite(0, 0);
			PA_DeleteSprite(0, 1);
			PA_DeleteSprite(0, 2);
			PA_DeleteSprite(0, 3);
			PA_DeleteSprite(0, 4);
			PA_DeleteSprite(0, 5);
			PA_DeleteSprite(0, 6);
			PA_DeleteSprite(0, 7);
			PA_DeleteSprite(0, 8);
			PA_DeleteSprite(0, 9);
			PA_DeleteSprite(0, 10);
			PA_DeleteSprite(0, 11);
			
			if (cb == 5 || temps - PA_VBLCounter[1]/60 <= 0)
			{
				if (cb == 5)
				{
					ch = atoi(total);
					if (ch > nb)
					{
						PA_DeleteBg(1, 0);
						PA_ClearTextBg(1);
						PA_ClearTextBg(0);
						PA_LoadBackground(1, 0, &moins);
						PA_OutputText(0, 0, 0, "Touche l'�cran!!!");		
						AS_MP3DirectPlay((u8*)moin, (u32)moin_size);
						PA_VBLCounterPause(1);
						PA_WaitFor(Stylus.Newpress);
						PA_VBLCounterStart(0);
						PA_ClearTextBg(0);
						AS_MP3Stop();
						cb = 0;
						ch = 0;
						sprintf(total, "");
					}
					else if  (ch < nb)
					{
						PA_DeleteBg(1, 0);
						PA_ClearTextBg(1);
						PA_ClearTextBg(0);
						PA_LoadBackground(1, 0, &plus);	
						AS_MP3DirectPlay((u8*)pluss, (u32)pluss_size);
						PA_OutputText(0, 0, 0, "Touche l'�cran!!!");
						PA_VBLCounterPause(1);
						PA_WaitFor(Stylus.Newpress);
						PA_VBLCounterStart(0);
						PA_ClearTextBg(0);
						AS_MP3Stop();
						cb = 0;
						ch = 0;
						sprintf(total, "");
					}
					else if (ch == nb)
					{
						PA_VBLCounterPause(1);	
						PA_DeleteSprite(0, 0);
						PA_DeleteSprite(0, 1);
						PA_DeleteSprite(0, 2);
						PA_DeleteSprite(0, 3);
						PA_DeleteSprite(0, 4);
						PA_DeleteSprite(0, 5);
						PA_DeleteSprite(0, 6);
						PA_DeleteSprite(0, 7);
						PA_DeleteSprite(0, 8);
						PA_DeleteSprite(0, 9);
						PA_CreateSprite(0, 10, (void*)oui_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 64);
						PA_CreateSprite(0, 11, (void*)non_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 128);
						PA_ClearTextBg(0);
						PA_ClearTextBg(1);
						PA_DeleteBg(1, 0);
						PA_LoadBackground(1, 0, &gagne);		
						PA_LoadBackground(0, 0, &rejouer);
						AS_MP3DirectPlay((u8*)gagnee, (u32)gagnee_size);
						PA_WaitFor(Stylus.Newpress);
						PA_ClearTextBg(0);
						AS_MP3Stop();
						etat = 4;
					}
				}
				if (temps - PA_VBLCounter[1]/60 <= 0)
				{
					PA_VBLCounterPause(1);	
					PA_DeleteSprite(0, 0);
					PA_DeleteSprite(0, 1);
					PA_DeleteSprite(0, 2);
					PA_DeleteSprite(0, 3);
					PA_DeleteSprite(0, 4);
					PA_DeleteSprite(0, 5);
					PA_DeleteSprite(0, 6);
					PA_DeleteSprite(0, 7);
					PA_DeleteSprite(0, 8);
					PA_DeleteSprite(0, 9);
					PA_CreateSprite(0, 10, (void*)oui_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 64);
					PA_CreateSprite(0, 11, (void*)non_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 128);
					PA_SetTextTileCol(1, TEXT_BLACK);
					PA_ClearTextBg(1);
					PA_ClearTextBg(0);
					PA_OutputText(1, 23, 20, "%d �", nb);
					PA_DeleteBg(1, 0);
					PA_LoadBackground(1, 2, &perdu);	
					PA_LoadBackground(0, 0, &rejouer);
					AS_MP3DirectPlay((u8*)perduu, (u32)perduu_size);
					PA_WaitFor(Stylus.Newpress);
					PA_ClearTextBg(0);
					AS_MP3Stop();
					etat = 4;
				}
			}
			else
			{	
				if (1)
				{
					PA_OutputText(0, 0, 4, "%d seconde(s) restante(s)", temps - PA_VBLCounter[1]/60);
					PA_OutputText(0, 0, 13, "                               ");
					PA_OutputText(0, 0, 13, "Votre prix: %s �", total);
					if (Pad.Newpress.Y) PA_OutputText(0, 0, 20, "R�ponse: %d EUROS!!!", nb);
				}
				PA_CreateSprite(0, 0, (void*)_0_Sprite, OBJ_SIZE_64X32, 1, 0, 0, 0);
				PA_CreateSprite(0, 1, (void*)_1_Sprite, OBJ_SIZE_64X32, 1, 0, 0, 64);
				PA_CreateSprite(0, 2, (void*)_2_Sprite, OBJ_SIZE_64X32, 1, 0, 0, 128);
				PA_CreateSprite(0, 3, (void*)_3_Sprite, OBJ_SIZE_64X32, 1, 0, 64, 0);
				PA_CreateSprite(0, 4, (void*)_4_Sprite, OBJ_SIZE_64X32, 1, 0, 64, 64);
				PA_CreateSprite(0, 5, (void*)_5_Sprite, OBJ_SIZE_64X32, 1, 0, 64, 128);
				PA_CreateSprite(0, 6, (void*)_6_Sprite, OBJ_SIZE_64X32, 1, 0, 128, 0);
				PA_CreateSprite(0, 7, (void*)_7_Sprite, OBJ_SIZE_64X32, 1, 0, 128, 64);
				PA_CreateSprite(0, 8, (void*)_8_Sprite, OBJ_SIZE_64X32, 1, 0, 128, 128);
				PA_CreateSprite(0, 9, (void*)_9_Sprite, OBJ_SIZE_64X32, 1, 0, 192, 0);
				
				if (Stylus.Newpress)
				{
					if (PA_SpriteTouched(0)) 
					{
						cb++;
						sprintf(total, "%s0", total);
					}
					else if (PA_SpriteTouched(1)) 
					{
						PA_WaitForVBL();
						sprintf(total, "%s1", total);
						cb++;
					}
					else if (PA_SpriteTouched(2)) 
					{
						sprintf(total, "%s2", total);
						cb++;
					}
					else if (PA_SpriteTouched(3)) 
					{
						sprintf(total, "%s3", total);
						cb++;
					}
					else if (PA_SpriteTouched(4)) 
					{
						sprintf(total, "%s4", total);
						cb++;
					}
					else if (PA_SpriteTouched(5)) 
					{
						sprintf(total, "%s5", total);
						cb++;
					}
					else if (PA_SpriteTouched(6)) 
					{
						sprintf(total, "%s6", total);
						cb++;
					}
					else if (PA_SpriteTouched(7)) 
					{
						sprintf(total, "%s7", total);
						cb++;
					}
					else if (PA_SpriteTouched(8)) 
					{
						sprintf(total, "%s8", total);
						cb++;
					}
					else if (PA_SpriteTouched(9)) 
					{
						sprintf(total, "%s9", total);
						cb++;
					}
				}
			}
		}
			
		if (Pad.Newpress.Start || PA_SpriteTouched(11) && etat == 1 || PA_SpriteTouched(11) && etat == 4) continuer = 0;
		
		if (Pad.Newpress.Select) PA_VBLCounter[1] = 0;
		
		PA_WaitForVBL();
	}
	
	return 0;
}