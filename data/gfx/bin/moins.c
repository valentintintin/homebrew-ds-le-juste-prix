#include <PA_BgStruct.h>

extern const char moins_Tiles[];
extern const char moins_Map[];
extern const char moins_Pal[];

const PA_BgStruct moins = {
	PA_BgNormal,
	256, 192,

	moins_Tiles,
	moins_Map,
	{moins_Pal},

	39360,
	{1536}
};
