#include <PA_BgStruct.h>

extern const char expliq_Tiles[];
extern const char expliq_Map[];
extern const char expliq_Pal[];

const PA_BgStruct expliq = {
	PA_BgNormal,
	256, 192,

	expliq_Tiles,
	expliq_Map,
	{expliq_Pal},

	39936,
	{1536}
};
