#include <PA_BgStruct.h>

extern const char proposez_Tiles[];
extern const char proposez_Map[];
extern const char proposez_Pal[];

const PA_BgStruct proposez = {
	PA_BgNormal,
	256, 192,

	proposez_Tiles,
	proposez_Map,
	{proposez_Pal},

	38720,
	{1536}
};
