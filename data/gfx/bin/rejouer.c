#include <PA_BgStruct.h>

extern const char rejouer_Tiles[];
extern const char rejouer_Map[];
extern const char rejouer_Pal[];

const PA_BgStruct rejouer = {
	PA_BgNormal,
	256, 192,

	rejouer_Tiles,
	rejouer_Map,
	{rejouer_Pal},

	46272,
	{1536}
};
