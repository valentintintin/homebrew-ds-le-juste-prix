#include <PA_BgStruct.h>

extern const char intro_Tiles[];
extern const char intro_Map[];
extern const char intro_Pal[];

const PA_BgStruct intro = {
	PA_BgNormal,
	256, 192,

	intro_Tiles,
	intro_Map,
	{intro_Pal},

	46400,
	{1536}
};
