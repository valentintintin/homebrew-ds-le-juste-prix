#include <PA_BgStruct.h>

extern const char gagne_Tiles[];
extern const char gagne_Map[];
extern const char gagne_Pal[];

const PA_BgStruct gagne = {
	PA_BgNormal,
	256, 192,

	gagne_Tiles,
	gagne_Map,
	{gagne_Pal},

	42240,
	{1536}
};
