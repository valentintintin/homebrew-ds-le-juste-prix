#include <PA_BgStruct.h>

extern const char plus_Tiles[];
extern const char plus_Map[];
extern const char plus_Pal[];

const PA_BgStruct plus = {
	PA_BgNormal,
	256, 192,

	plus_Tiles,
	plus_Map,
	{plus_Pal},

	39104,
	{1536}
};
