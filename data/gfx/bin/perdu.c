#include <PA_BgStruct.h>

extern const char perdu_Tiles[];
extern const char perdu_Map[];
extern const char perdu_Pal[];

const PA_BgStruct perdu = {
	PA_BgNormal,
	256, 192,

	perdu_Tiles,
	perdu_Map,
	{perdu_Pal},

	40256,
	{1536}
};
