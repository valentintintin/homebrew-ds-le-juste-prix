// Graphics converted using PAGfx by Mollusk.

#pragma once

#include <PA_BgStruct.h>

#ifdef __cplusplus
extern "C"{
#endif

// Sprites:
extern const unsigned char _0_Sprite[2048] _GFX_ALIGN; // Palette: _0_Pal
extern const unsigned char _1_Sprite[2048] _GFX_ALIGN; // Palette: _1_Pal
extern const unsigned char _2_Sprite[2048] _GFX_ALIGN; // Palette: _2_Pal
extern const unsigned char _3_Sprite[2048] _GFX_ALIGN; // Palette: _3_Pal
extern const unsigned char _4_Sprite[2048] _GFX_ALIGN; // Palette: _4_Pal
extern const unsigned char _5_Sprite[2048] _GFX_ALIGN; // Palette: _5_Pal
extern const unsigned char _6_Sprite[2048] _GFX_ALIGN; // Palette: _6_Pal
extern const unsigned char _7_Sprite[2048] _GFX_ALIGN; // Palette: _7_Pal
extern const unsigned char _8_Sprite[2048] _GFX_ALIGN; // Palette: _8_Pal
extern const unsigned char _9_Sprite[2048] _GFX_ALIGN; // Palette: _9_Pal
extern const unsigned char non_Sprite[2048] _GFX_ALIGN; // Palette: non_Pal
extern const unsigned char oui_Sprite[2048] _GFX_ALIGN; // Palette: oui_Pal

// Backgrounds:
extern const PA_BgStruct expliq;
extern const PA_BgStruct gagne;
extern const PA_BgStruct intro;
extern const PA_BgStruct moins;
extern const PA_BgStruct perdu;
extern const PA_BgStruct plus;
extern const PA_BgStruct rejouer;
extern const PA_BgStruct proposez;

// Palettes:
extern const unsigned short _0_Pal[256] _GFX_ALIGN;
extern const unsigned short _1_Pal[256] _GFX_ALIGN;
extern const unsigned short _2_Pal[256] _GFX_ALIGN;
extern const unsigned short _3_Pal[256] _GFX_ALIGN;
extern const unsigned short _4_Pal[256] _GFX_ALIGN;
extern const unsigned short _5_Pal[256] _GFX_ALIGN;
extern const unsigned short _6_Pal[256] _GFX_ALIGN;
extern const unsigned short _7_Pal[256] _GFX_ALIGN;
extern const unsigned short _8_Pal[256] _GFX_ALIGN;
extern const unsigned short _9_Pal[256] _GFX_ALIGN;
extern const unsigned short non_Pal[256] _GFX_ALIGN;
extern const unsigned short oui_Pal[256] _GFX_ALIGN;

#ifdef __cplusplus
}
#endif
